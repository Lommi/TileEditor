#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include "globals.h"
#include "graphics.h"
#include "input.h"
#include "game.h"

Game game;
bool g_mainloop;
int  SCREEN_WIDTH;
int  SCREEN_HEIGHT;
int  TILE_SIZE;

int main(int argc, char **argv)
{

    game.gamestate = GAMESTATE_ACTIVE;
    SCREEN_WIDTH  = 1600;
    SCREEN_HEIGHT = 900;
    TILE_SIZE     = 32;
    g_mainloop    = 1;

    Input    input;
    Renderer renderer;
    Assets   assets;
    World    world;

    if (renderer.init("Seikkailumaailma") != 0) printf("Error in render_init\n");

    renderer.create_viewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, assets.c_background, "main");
    assets.load_texture(renderer.renderer, "../assets/tileset2.png");
    assets.load_font(renderer.renderer, "../assets/consola.ttf", 16);
    assets.set_tileset_index(0);
    assets.generate_tileset_from_texture(assets.textures[0], TILE_SIZE);
    world.create_room(100, 100); //TODO: Fix sizes that are not the same
    world.current_room = world.rooms[0];
    world.current_room->viewport = renderer.viewports[0];
    world.load_characters(renderer.renderer, &assets);
    world.load_room("ebin");

    while (g_mainloop)
    {
        input.game_update(&game, &world, &assets);
        renderer.game_update(&game, &world, &assets);
    }

    return 0;
}
