#ifndef INPUT_H
#define INPUT_H
#include <SDL2/SDL.h>
#include <iostream>
#include "game.h"
#include "graphics.h"

class Input
{
    public:
    bool l_mb_down  = 0;
    bool r_mb_down  = 0;
    bool l_shift_down = 0;
    bool input_active = 0;

    int editor_xview = 0;
    int editor_yview = 0;
    int tools_xview  = 0;
    int tools_yview  = 0;
    int fixedpos     = 0;
    int rows         = 0;

    void game_update(Game *game, World *world, Assets *assets);
    void editor_update(Game *game, World *world, Assets *assets);
};
#endif
