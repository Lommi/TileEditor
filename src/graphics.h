#ifndef GRAPHICS_H
#define GRAPHICS_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <vector>

using namespace std;

struct Color{ uint8_t r, g, b, a; };

class Assets;
class Renderer;
class Viewport;
class Texture;
class World;
class Button;

class Button
{
    public:
    Button()
    {
        fill = new SDL_Rect;
        bg = new SDL_Rect;
    };
    ~Button(){ delete fill; delete bg; };
    SDL_Rect *bg;
    SDL_Rect *fill;
    void render(SDL_Renderer *renderer, Assets *assets);
    char text[16] = {0};
    bool pressed(int mouse_x, int mouse_y);
};

class Texture
{
    public:
    Texture(){ tex = 0; }
    ~Texture(){ free(); }
    bool load(SDL_Renderer *renderer, string path);
    void free();
    void render(SDL_Renderer *renderer, int x, int y, SDL_Rect *clip = 0);
    void render_ex(SDL_Renderer *renderer, int x, int y, SDL_Rect *clip, double angle, SDL_Point *center, SDL_RendererFlip flip);
    void render_text(SDL_Renderer *renderer, int x, int y, string text,  TTF_Font *font, SDL_Color color);
    void set_color(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
    void set_blend(SDL_BlendMode blending);
    SDL_Texture *tex  = 0;
    SDL_Rect    *clip = 0;
};

class Viewport
{
    public:
    Viewport(){ rec = 0; };
    ~Viewport(){ delete rec; };

    SDL_Rect *rec;
    Color    bg_color;
    string   name;
};

class Renderer
{
    public:

    SDL_Window       *window;
    SDL_Renderer     *renderer;
    SDL_Rect         display_rec;
    vector<Viewport*> viewports;

    Renderer()
    {
        window       = 0;
        renderer     = 0;
    }
    ~Renderer()
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        window   = 0;
        renderer = 0;

        IMG_Quit();
        SDL_Quit();

        printf("render_shutdown()\n");
    }
    int  init(const char *windowname);
    int  create_viewport(int x, int y, int w, int h, Color bg_color, string name);
    void game_update(Game *game, World *world, Assets *assets);
    void editor_update(Game *game, World *world, Assets *assets);
    SDL_Rect get_display_size(int display);
};

class Assets
{
    public:
    Assets(){};
    ~Assets(){};
    vector<Texture*>  textures;
    vector<Texture*>  rendertexts;
    vector<TTF_Font*> fonts;
    vector<SDL_Rect*> tileset_clips;
    vector<Button*> buttons;

    Color c_red         = {255, 0, 0, 255};
    Color c_green       = {0, 255, 0, 255};
    Color c_blue        = {0, 0, 255, 255};
    Color c_white       = {255, 255, 255, 255};
    Color c_black       = {0, 0, 0, 255};
    Color c_background  = {112, 136, 172, 255};

    SDL_Color sdl_white = {255, 255, 255};
    SDL_Color sdl_black = {0, 0, 0};

    int tileset_index;
    int num_tileset_tiletypes;

    int load_texture(SDL_Renderer *renderer, string path);
    int load_font(SDL_Renderer *renderer, string path, int size);
    int generate_tileset_from_texture(Texture *tex, int tilesize);
    int create_button(int x, int y, int w, int h, const char *text);
    void set_tileset_index(int i){ tileset_index = i; };
};

#endif
