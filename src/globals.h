#ifndef GLOBALS_H
#define GLOBALS_H
#include <cstdint>

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

extern bool g_mainloop;
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern int TILE_SIZE;

enum GameState
{
    GAMESTATE_MENU,
    GAMESTATE_ACTIVE
};

class Game
{
    public:
    GameState gamestate = GAMESTATE_MENU;
};

#endif
