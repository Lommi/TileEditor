#include "globals.h"
#include "input.h"

void Input::game_update(Game *game, World *world, Assets *assets)
{
    SDL_Event e;
    while(SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case (SDL_QUIT):
            {
                g_mainloop = 0;
            }break;
            case (SDL_KEYDOWN):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_ESCAPE: g_mainloop = 0; break;
                    case SDLK_a:
                    {
                        if (world->current_room->characters[0]->x > 0)
                            world->current_room->characters[0]->x -= 32;
                    }break;
                    case SDLK_w:
                    {
                        if (world->current_room->characters[0]->y > 0)
                            world->current_room->characters[0]->y -= 32;
                    }break;
                    case SDLK_d:
                    {
                        if (world->current_room->characters[0]->x < (world->current_room->width * TILE_SIZE) -
                                (world->current_room->viewport->rec->w / TILE_SIZE) * TILE_SIZE)
                            world->current_room->characters[0]->x += 32;
                    }break;
                    case SDLK_s:
                    {
                        if (world->current_room->characters[0]->y < (world->current_room->height * TILE_SIZE) -
                                (world->current_room->viewport->rec->h / TILE_SIZE) * TILE_SIZE)
                            world->current_room->characters[0]->y += 32;
                    }break;
                }
            }

        }
    }
}

void Input::editor_update(Game *game, World *world, Assets *assets)
{
    SDL_Event e;
    while(SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case (SDL_QUIT):
            {
                g_mainloop = 0;
            }break;
            case (SDL_TEXTINPUT):
            {
                if (input_active)
                {
                    if (world->name.size() < 16)
                        world->name.append(e.text.text);
                }
            }break;
            case (SDL_KEYDOWN):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_BACKSPACE:
                    {
                        if (input_active && world->name.length() > 0) world->name.pop_back();

                    }break;
                    case SDLK_RETURN:
                    {
                        if (input_active)
                        {
                            input_active = 0;
                        }
                        else
                            input_active = 1;
                    }break;
                    case SDLK_ESCAPE:
                    {
                        g_mainloop = 0;
                    }break;
                    case SDLK_LSHIFT:
                    {
                        l_shift_down = 1;
                    }break;
                    case SDLK_a:
                    {
                        if (world->current_room->view->x > 0)
                            world->current_room->view->x -= 32;
                    }break;
                    case SDLK_w:
                    {
                        if (world->current_room->view->y > 0)
                            world->current_room->view->y -= 32;
                    }break;
                    case SDLK_d:
                    {
                        if (world->current_room->view->x < (world->current_room->width * TILE_SIZE) -
                                (world->current_room->viewport->rec->w / TILE_SIZE) * TILE_SIZE)
                            world->current_room->view->x += 32;
                    }break;
                    case SDLK_s:
                    {
                        if (world->current_room->view->y < (world->current_room->height * TILE_SIZE) -
                                (world->current_room->viewport->rec->h / TILE_SIZE) * TILE_SIZE)
                            world->current_room->view->y += 32;
                    }break;
                    case SDLK_1: world->editor_state = 0; break;
                    case SDLK_2: world->editor_state = 1; break;
                    case SDLK_3: world->editor_state = 2; break;
                    case SDLK_4: world->editor_state = 3; break;
                }
            }break;
            case (SDL_KEYUP):
            {
                case SDLK_LSHIFT:
                {
                    l_shift_down = 0;
                }break;
            }break;
            case (SDL_MOUSEBUTTONDOWN):
            {
                switch (e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        if (l_shift_down) l_mb_down = 1;
                        SDL_GetMouseState(&e.button.x, &e.button.y);
                        editor_xview = world->current_room->viewport->rec->w;
                        editor_yview = world->current_room->viewport->rec->h;
                        tools_xview = SCREEN_WIDTH - assets->textures[assets->tileset_index]->clip->w;
                        tools_yview = assets->textures[assets->tileset_index]->clip->h;
                        if (e.button.x < editor_xview && e.button.y < editor_yview)
                        {
                            if (world->editor_state == 0)
                            {
                                world->current_room->tiles[world->get_tile_from_mouse_pos(
                                    &e.button.x, &e.button.y,
                                    world->current_room->view->x / TILE_SIZE,
                                    world->current_room->view->y / TILE_SIZE,
                                    world->current_room->height, 0)] = world->editor_tile;
                            }
                            else if (world->editor_state == 1)
                            {
                                int x, y;
                                x = world->current_room->view->x + ((e.button.x / TILE_SIZE) * TILE_SIZE);
                                y = world->current_room->view->y + ((e.button.y / TILE_SIZE) * TILE_SIZE);
                                if (world->selected_character >= 0 &&
                                    world->selected_character <= world->characters.size() - 1)
                                world->current_room->add_character(
                                    world->characters[world->selected_character], x, y);
                            }
                        }
                        else if (e.button.x > tools_xview && e.button.y < tools_yview)
                        {
                            fixedpos = e.button.x - tools_xview;
                            rows = assets->textures[assets->tileset_index]->clip->h / TILE_SIZE;
                            world->editor_tile = world->get_tile_from_mouse_pos(
                                &fixedpos, &e.button.y,
                                0, 0, rows + 2 , 0);
                            world->editor_tile_x = tools_xview + (fixedpos / TILE_SIZE) * TILE_SIZE;
                            world->editor_tile_y = (e.button.y / TILE_SIZE) * TILE_SIZE;
                        }
                        for (int i = 0; i < assets->buttons.size(); ++i)
                        {
                            if (assets->buttons[i]->pressed(e.button.x, e.button.y))
                            {
                                printf("Button %s pressed!\n", assets->buttons[i]->text);
                                input_active = 0;
                                switch(i)
                                {
                                    case 0: world->editor_state = 0; break;
                                    case 1: world->editor_state = 1; break;
                                    case 2: world->editor_state = 2; break;
                                    case 3: world->editor_state = 3; break;
                                    case 4: world->save_room(); break;
                                    case 5: world->load_room(world->current_room->name); break;
                                }
                            }
                        }
                    }break;
                    case SDL_BUTTON_RIGHT:

                        if (l_shift_down) r_mb_down = 1;

                        SDL_GetMouseState(&e.button.x, &e.button.y);
                        editor_xview = world->current_room->viewport->rec->w;
                        tools_xview = SCREEN_WIDTH - assets->textures[assets->tileset_index]->clip->w;
                        tools_yview = assets->textures[assets->tileset_index]->clip->h;
                        if (e.button.x > tools_xview && e.button.y < tools_yview)
                        {
                            fixedpos = e.button.x - tools_xview;
                            rows = assets->textures[assets->tileset_index]->clip->h / TILE_SIZE;
                            world->editor_tile2 = world->get_tile_from_mouse_pos(
                                &fixedpos, &e.button.y,
                                0, 0, rows + 2, 1);
                            world->editor_tile2_x = tools_xview + (fixedpos / TILE_SIZE) * TILE_SIZE;
                            world->editor_tile2_y = (e.button.y / TILE_SIZE) * TILE_SIZE;
                        }
                        if (e.button.x < editor_xview && e.button.y < editor_yview &&
                                world->editor_state == 0)
                        {
                            world->current_room->tiles[world->get_tile_from_mouse_pos(&e.button.x, &e.button.y,
                            world->current_room->view->x / TILE_SIZE,
                            world->current_room->view->y / TILE_SIZE,
                            world->current_room->height, 0)] = world->editor_tile2;
                        }
                        else if (e.button.x < editor_xview && e.button.y < editor_yview &&
                            world->editor_state == 1)
                        {
                            for (int i = 0; i < world->current_room->characters.size(); ++i)
                            {
                                Character *c = world->current_room->characters[i];
                                printf("mousex: %d mousey: %d c->x: %d c->y: %d c->w: %d c->h: %d\n",
                                    world->current_room->view->x + e.button.x,
                                    world->current_room->view->y + e.button.y,
                                    c->x, c->y, c->x + c->tex->clip->w, c->y + c->tex->clip->h);

                                if (c->x <= world->current_room->view->x + e.button.x &&
                                    c->x + c->tex->clip->w >= world->current_room->view->x + e.button.x &&
                                    c->y <= world->current_room->view->y + e.button.y &&
                                    c->y + c->tex->clip->h >= world->current_room->view->y + e.button.y)
                                {
                                    printf("c->id: %d\n", c->id);
                                    world->current_room->characters.erase(
                                        world->current_room->characters.begin() + i);
                                }
                            }
                        }
                    break;
                }
            }break;
            case (SDL_MOUSEBUTTONUP):
            {
                switch (e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                        l_mb_down = 0;
                    break;
                    case SDL_BUTTON_RIGHT:
                        r_mb_down = 0;
                    break;
                }
            }break;
            case (SDL_MOUSEWHEEL):
            {
                if (e.wheel.y == 1)
                {
                    if (world->selected_character < world->characters.size() - 1)
                        ++world->selected_character;
                    printf("selected_character = %d\n", world->selected_character);
                }
                else if (e.wheel.y == -1)
                {
                    if (world->selected_character > 0)
                        --world->selected_character;
                    printf("selected_character = %d\n", world->selected_character);
                }

                printf("%d\n", world->editor_tile);
            }break;
        }
    }
    if (world->editor_state == 0)
    {
        if (r_mb_down)
        {
            editor_xview = world->current_room->viewport->rec->w;
            editor_yview = world->current_room->viewport->rec->h;
            SDL_GetMouseState(&e.button.x, &e.button.y);
            if (e.button.x < editor_xview && e.button.y < editor_yview)
            {
                world->current_room->tiles[world->get_tile_from_mouse_pos(&e.button.x, &e.button.y,
                world->current_room->view->x / TILE_SIZE,
                world->current_room->view->y / TILE_SIZE,
                world->current_room->height, 0)] = world->editor_tile2;
            }
        }
        else if (l_mb_down)
        {
            SDL_GetMouseState(&e.button.x, &e.button.y);
            editor_xview = world->current_room->viewport->rec->w;
            editor_yview = world->current_room->viewport->rec->h;
            tools_xview = SCREEN_WIDTH - assets->textures[assets->tileset_index]->clip->w;
            if (e.button.x < editor_xview && e.button.y < editor_yview)
            {
                world->current_room->tiles[world->get_tile_from_mouse_pos(
                    &e.button.x, &e.button.y,
                    world->current_room->view->x / TILE_SIZE,
                    world->current_room->view->y / TILE_SIZE,
                    world->current_room->height, 0)] = world->editor_tile;
            }
        }
    }
}
