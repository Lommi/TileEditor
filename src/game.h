#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "graphics.h"

using namespace std;

class World;
class Room;
class View;
class Characrer;

class Character
{
    public:
    Character(){};
    ~Character(){};
    Texture *tex;
    string name;
    string likes;
    int id, hp, str;
    int x, y;
};

class View
{
    public:
    View(){ x = 0; y = 0; w = 0; h = 0; };
    ~View(){};
    int x, y, w, h;
};

class Room
{
    public:
    Room(){ view = new View; };
    ~Room(){ delete view; };
    vector<unsigned int> tiles;
    vector<Character *> characters;
    int  width, height;
    string name;
    View *view;
    Viewport *viewport;
    int add_character(Character *c, int x, int y);
};

class World
{
    public:
    World()
    {
        initialized = 0;
        size = 0;
        srand(time(0));
        editor_state = 0;
        editor_tile   = 1;
        editor_tile_x = 0;
        editor_tile_y = 0;
        editor_tile2   = 0;
        editor_tile2_x = 0;
        editor_tile2_y = 0;
        selected_character = 0;
        f = 0;
    };
    ~World(){ if (f) fclose(f);};
    vector<Room*> rooms;
    vector<Character*> characters;
    Room *current_room;
    FILE *f;
    int editor_state;
    int editor_tile_x, editor_tile_y;
    int editor_tile2_x, editor_tile2_y;
    int initialized;
    unsigned int selected_character;
    unsigned int size;
    unsigned int editor_tile;
    unsigned int editor_tile2;
    string name;

    int create_room(int room_w, int room_h);
    int save_room();
    int load_room(string name);
    unsigned int get_tile_from_mouse_pos(int *mouse_x, int *mouse_y,
        int offset_x, int offset_y, int rows, bool hack);
    int load_characters(SDL_Renderer *renderer, Assets* assets);
};

#endif
