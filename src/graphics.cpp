#include "globals.h"
#include "graphics.h"
#include "game.h"

int Renderer::init(const char *windowname)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL_Init FAIL!, SDL Error: %s\n", SDL_GetError());
        return 1;
    }

	//Set texture filtering to linear
	if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
	{
		printf( "Warning: Linear texture filtering not enabled!" );
	}

    display_rec = get_display_size(0);
    if (display_rec.w < 1600)
    {
        SCREEN_WIDTH = 1366;
        SCREEN_HEIGHT = 768;
    }
    printf("current_display_w: %d, current_display_h: %d\n", SCREEN_WIDTH, SCREEN_HEIGHT);

    window = SDL_CreateWindow(windowname, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (!window)
    {
        printf("SDL_CreateWindow FAIL!, SDL Error: %s\n", SDL_GetError());
        return 2;
    }

    renderer = SDL_CreateRenderer(window, -1,
               SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (!renderer)
    {
        printf("SDL_CreateRenderer FAIL!, SDL Error: %s\n", SDL_GetError());
        return 3;
    }
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags))
    {
        printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
        return 4;
    }

    if (TTF_Init() == -1)
    {
        printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
        return 5;
    }

    printf("render_init()\n");
    return 0;
}

void Renderer::game_update(Game *game, World *world, Assets *assets)
{
    int row = 0, col = 0, x = 0, y = 0;

    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    SDL_RenderSetViewport(renderer, viewports[0]->rec);
    SDL_SetRenderDrawColor(renderer,
        viewports[0]->bg_color.r, viewports[0]->bg_color.g,
        viewports[0]->bg_color.b, viewports[0]->bg_color.a);
    SDL_RenderFillRect(renderer, viewports[0]->rec);

    for (unsigned int i = 0; i < world->current_room->tiles.size(); ++i)
    {
        row = i % world->current_room->width;
        col = i / world->current_room->height;
        x = (-world->current_room->view->x) + row * (TILE_SIZE);
        y = (-world->current_room->view->y) + col * (TILE_SIZE);

        assets->textures[assets->tileset_index]->render(renderer, x, y,
            assets->tileset_clips[world->current_room->tiles[i]]);
    }
    for (int i = 0; i < world->current_room->characters.size(); ++i)
    {
        x = -world->current_room->view->x +
            world->current_room->characters[i]->x;
        y = -world->current_room->view->y +
            world->current_room->characters[i]->y;
        world->current_room->characters[i]->tex->render(renderer,
        x, y, world->current_room->characters[i]->tex->clip);
    }

    SDL_RenderPresent(renderer);
}

void Renderer::editor_update(Game *game, World *world, Assets *assets)
{
    int row = 0, col = 0, x = 0, y = 0;

    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    for (int i = 0; i < viewports.size(); i++)
    {
        SDL_RenderSetViewport(renderer, viewports[i]->rec);

        SDL_SetRenderDrawColor(renderer,
            viewports[i]->bg_color.r, viewports[i]->bg_color.g,
            viewports[i]->bg_color.b, viewports[i]->bg_color.a);
        SDL_RenderFillRect(renderer, viewports[i]->rec);

        if (i == 0)
        {
            switch (world->editor_state)
            {
                case 0:
                {
                    assets->textures[assets->tileset_index]->render(renderer,
                        SCREEN_WIDTH - assets->textures[assets->tileset_index]->clip->w, 0,
                        assets->textures[assets->tileset_index]->clip);

                    // Selected tile indicator
                    assets->textures[1]->render(renderer,
                        world->editor_tile_x, world->editor_tile_y,
                        assets->textures[1]->clip);
                    assets->textures[2]->render(renderer,
                        world->editor_tile2_x, world->editor_tile2_y,
                        assets->textures[2]->clip);

                }break;
                case 1:
                {
                    for (int i = 0; i < world->characters.size(); ++i)
                    {
                        if (world->selected_character <= world->characters.size() - 1)
                            world->characters[i]->tex->render(renderer,
                                SCREEN_WIDTH - 300 - (64 * (world->selected_character - i)), 64,
                                world->characters[i]->tex->clip);
                    }
                    if (world->selected_character <= world->characters.size() - 1)
                        assets->rendertexts[0]->render_text(renderer,
                            SCREEN_WIDTH - 300, 16,
                            world->characters[world->selected_character]->name,
                            assets->fonts[0], assets->sdl_white);
                }break;
            }
            assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 600, SCREEN_HEIGHT - 16, "EDITOR STATE:", assets->fonts[0], assets->sdl_white);
            switch(world->editor_state)
            {
                case 0: assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 474, SCREEN_HEIGHT - 16, "TILES", assets->fonts[0], assets->sdl_white);
                break;
                case 1: assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 474, SCREEN_HEIGHT - 16, "CHARS", assets->fonts[0], assets->sdl_white);
                break;
                case 2: assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 474, SCREEN_HEIGHT - 16, "ITEMS", assets->fonts[0], assets->sdl_white);
                break;
                case 3: assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 474, SCREEN_HEIGHT - 16, "DOORS", assets->fonts[0], assets->sdl_white);
                break;
            }

            assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 256, SCREEN_HEIGHT - 16, "ROOM NAME:", assets->fonts[0],
                assets->sdl_white);
            assets->rendertexts[0]->render_text(renderer,
                SCREEN_WIDTH - 156, SCREEN_HEIGHT - 16, world->name.c_str(), assets->fonts[0],
                assets->sdl_white);

            for (int i = 0; i < assets->buttons.size(); ++i)
                assets->buttons[i]->render(renderer, assets);
        }

        if (i == 1)
        {
            for (unsigned int i = 0; i < world->current_room->tiles.size(); ++i)
            {
                row = i % world->current_room->width;
                col = i / world->current_room->height;
                x = (-world->current_room->view->x) + row * (TILE_SIZE);
                y = (-world->current_room->view->y) + col * (TILE_SIZE);

                assets->textures[assets->tileset_index]->render(renderer, x, y,
                    assets->tileset_clips[world->current_room->tiles[i]]);
            }
            for (int i = 0; i < world->current_room->characters.size(); ++i)
            {
                x = -world->current_room->view->x +
                    world->current_room->characters[i]->x;
                y = -world->current_room->view->y +
                    world->current_room->characters[i]->y; /*
                x = world->current_room->characters[i]->x;
                y = world->current_room->characters[i]->y;*/
                world->current_room->characters[i]->tex->render(renderer,
                x, y, world->current_room->characters[i]->tex->clip);
            }
        }
    }

    SDL_RenderPresent(renderer);
}

int Renderer::create_viewport(int x, int y, int w, int h, Color bg_color, string name)
{
    Viewport *new_vp = new Viewport;
    SDL_Rect *rec = new SDL_Rect;

    rec->x = x;
    rec->y = y;
    rec->w = w;
    rec->h = h;

    new_vp->rec = rec;
    new_vp->name = name;
    new_vp->bg_color = bg_color;

    viewports.push_back(new_vp);
    printf("Created new viewport \"%s\"\nx: %d, y: %d, w: %d, h: %d\nviewports.size = %d\n", new_vp->name.c_str(),rec->x, rec->y, rec->w, rec->h, viewports.size());

    return 0;
}

SDL_Rect Renderer::get_display_size(int display)
{
    SDL_Rect rec;
    SDL_GetDisplayBounds(display, &rec);
    printf("screen_max_w: %d, screen_max_h: %d\n", rec.w, rec.h);
    return rec;
}

void Texture::render(SDL_Renderer *renderer, int x, int y, SDL_Rect *clip)
{
    SDL_Rect renderquad;
    if (clip)
    {
        renderquad.x = x;
        renderquad.y = y;
        renderquad.w = clip->w;
        renderquad.h = clip->h;
    }
    SDL_RenderCopy(renderer, tex, clip, &renderquad);
}

void Texture::render_ex(SDL_Renderer *renderer, int x, int y, SDL_Rect *clip, double angle, SDL_Point *center, SDL_RendererFlip flip)
{
    SDL_Rect renderquad;
    if (clip)
    {
        renderquad.x = x;
        renderquad.y = y;
        renderquad.w = clip->w;
        renderquad.h = clip->h;
    }
    SDL_RenderCopyEx(renderer, tex, clip, &renderquad, angle, center, flip);
}

void Texture::render_text(SDL_Renderer *renderer, int x, int y, string text, TTF_Font *font, SDL_Color color)
{
    if (text.length() == 0) return;
    free();
    SDL_Rect renderquad;
    SDL_Surface *surface = TTF_RenderText_Solid(font, text.c_str(), color);
    tex = SDL_CreateTextureFromSurface(renderer, surface);
    clip->x = 0;
    clip->y = 0;
    clip->w = surface->w;
    clip->h = surface->h;
    if (clip)
    {
        renderquad.x = x;
        renderquad.y = y;
        renderquad.w = clip->w;
        renderquad.h = clip->h;
    }
    SDL_FreeSurface(surface);
    SDL_RenderCopy(renderer, tex, clip, &renderquad);
}

bool Texture::load(SDL_Renderer *renderer, string path)
{
    free();

    SDL_Texture *new_tex = 0;
    SDL_Rect    *new_rec = 0;

    SDL_Surface *surface = IMG_Load(path.c_str());
    if (!surface)
        printf("Image load failed %s! SDL_image_error: %s\n", path.c_str(), SDL_GetError());
    else
    {
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0xFF, 0xFF));
        new_tex = SDL_CreateTextureFromSurface(renderer, surface);
        if (!new_tex)
            printf("Texture creation failed from %s! SDL Error %s\n", path.c_str(), SDL_GetError());
        else
        {
            new_rec->w = surface->w;
            new_rec->h = surface->h;
        }
        SDL_FreeSurface(surface);
    }
    tex  = new_tex;
    clip = new_rec;

    return tex != 0;
}

void Texture::free()
{
    if (tex)
    {
        SDL_DestroyTexture(tex);
        tex = 0;
    }
}

void Texture::set_color(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    SDL_SetTextureColorMod(tex, r, g, b);
    SDL_SetTextureAlphaMod(tex, a);
}

void Texture::set_blend(SDL_BlendMode blending)
{
    SDL_SetTextureBlendMode(tex, blending);
}

int Assets::load_texture(SDL_Renderer *renderer, string path)
{
    Texture  *tex  = new Texture;
    SDL_Rect *clip = new SDL_Rect;
    SDL_Surface *surface = IMG_Load(path.c_str());

    if (!surface)
        printf("Image load failed %s! SDL_image_error: %s\n", path.c_str(), SDL_GetError());
    else
    {
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0xFF, 0xFF));
        tex->tex = SDL_CreateTextureFromSurface(renderer, surface);
        if (!tex->tex)
            printf("Texture creation failed %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        else
        {
            clip->x = 0;
            clip->y = 0;
            clip->w = surface->w;
            clip->h = surface->h;

            tex->clip = clip;
        }
        SDL_FreeSurface(surface);
        printf("Texture %s load succesfull!\n", path.c_str());
        textures.push_back(tex);
    }
    printf("size: %d\n", textures.size());

    return 0;
}

int Assets::load_font(SDL_Renderer *renderer, string path, int size)
{
    Texture  *tex = new Texture;
    SDL_Rect *clip = new SDL_Rect;
    SDL_Color color = { 255, 255, 255 };
    TTF_Font* font = 0;
    font = TTF_OpenFont(path.c_str(), size);

    SDL_Surface *surface = TTF_RenderText_Solid(font, " ", color);

    if (!font)
    {
        printf("Font %s load failed!\nSDL_ttf Error: %s\n", path.c_str(), TTF_GetError());
        return 1;
    }
    else
    {
        //font_default = font;
        fonts.push_back(font);
        printf("Font %s load succesful!\n", path.c_str());
    }

    if (!surface)
    {
        printf("surface load failed %s\nSDL_ttf Error: %s\n", path.c_str(), TTF_GetError());
        return 2;
    }
    else
    {
        tex->tex = SDL_CreateTextureFromSurface(renderer, surface);
        if (!tex->tex)
        {
            printf("texture load failed %s\n SDL Error: %s\n", path.c_str(), SDL_GetError());
            return 3;
        }
        else
        {
            clip->x = 0;
            clip->y = 0;
            clip->w = surface->w;
            clip->h = surface->h;
            tex->clip = clip;
        }
        SDL_FreeSurface(surface);
        rendertexts.push_back(tex);
        printf("rendertex load succesfull!\n");
    }

    return 0;
}

int Assets::generate_tileset_from_texture(Texture *tex, int tilesize)
{
    if (tex == 0)
    {
        printf("Texture not found at generate_tileset_from_texture()\n");
        return 1;
    }
    if (tilesize == 0)
    {
        printf("tilesize can't be 0 at generate_tileset_from_texture()\n");
        return 2;
    }

    int rows = tex->clip->h / tilesize;
    int cols = tex->clip->w / tilesize;
    int num_clips = rows * cols;
    num_tileset_tiletypes = num_clips;

    for ( int i = 0; i < rows; ++i)
    {
        for ( int j = 0; j < cols; ++j)
        {
            SDL_Rect *new_rec = new SDL_Rect;

            new_rec->x = tilesize * j;
            new_rec->y = tilesize * i;
            new_rec->w = tilesize;
            new_rec->h = tilesize;

            tileset_clips.push_back(new_rec);
        }
    }

    return 0;
}

int Assets::create_button(int x, int y, int w, int h, const char *text)
{
    Button *button = new Button;

    button->fill->x = x;
    button->fill->y = y;
    button->fill->w = w;
    button->fill->h = h;
    button->bg->x = x-1;
    button->bg->y = y-1;
    button->bg->w = w+2;
    button->bg->h = h+2;

    strcpy(button->text, text);

    buttons.push_back(button);

    return 0;
}

void Button::render(SDL_Renderer *renderer, Assets *assets)
{
    if (fill && renderer)
    {
        SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
        SDL_RenderFillRect(renderer, bg);
        SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderFillRect(renderer, fill);
        assets->rendertexts[0]->render_text(renderer,
            fill->x + 4, fill->y + 4, text, assets->fonts[0], assets->sdl_black);
    }
}

bool Button::pressed(int mouse_x, int mouse_y)
{
    if (mouse_x >= bg->x &&
        mouse_x <= bg->x + bg->w &&
        mouse_y >= bg->y &&
        mouse_y <= bg->y + bg->h) return 1;
    else return 0;
}
