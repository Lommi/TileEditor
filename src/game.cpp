#include "globals.h"
#include "game.h"

int World::create_room(int room_w, int room_h)
{
    uint8_t tile;

    Room *room = new Room;
    Viewport *viewport = new Viewport;
    room->width = room_w;
    room->height = room_h;
    room->view->w = room_w;
    room->view->h = room_h;
    room->viewport = viewport;

    for (int i = 0; i < room_w; i++)
    {
        for (int j = 0; j < room_h; j++)
        {
            tile = 0;
            room->tiles.push_back(tile);
        }
    }

    rooms.push_back(room);

    return 0;
}

unsigned int World::get_tile_from_mouse_pos(int *mouse_x, int *mouse_y,
    int offset_x, int offset_y, int rows, bool hack)
{
    int tile_x, tile_y;
    unsigned int tile = -1;

    tile_x = offset_x + (*mouse_x / TILE_SIZE);
    tile_y = offset_y + (*mouse_y / TILE_SIZE);
    tile = tile_x + tile_y * rows;
    printf("tile: %d\n", tile);
    return tile;
}

int World::save_room()
{
    if (name.size() < 1)
    {
        printf("World name can't be empty!\n");
        return 1;
    }
    char buf[64];
    char line[512];
    int val;
    int idval;
    sprintf(buf, "../maps/%s.bin", name.c_str());
    f = fopen(buf, "wb");
    if (!f)
    {
        printf("Could not save %s\n", buf);
        fclose(f);
        return 2;
    }
    fwrite(&current_room->tiles[0], sizeof(vector<unsigned int>::value_type), current_room->tiles.size(), f);
    fclose(f);
    printf("Tile data %s saved\n", name.c_str());

    sprintf(buf, "../maps/%s_characters.txt", name.c_str());
    f = fopen(buf, "w+");
    if (!f)
    {
        printf("Could not load %s\n", buf);
        fclose(f);
        return 2;
    }
    for (int i = 0; i < current_room->characters.size(); ++i)
    {
        sprintf(buf, "%d %d %d\n",
            current_room->characters[i]->id,
            current_room->characters[i]->x,
            current_room->characters[i]->y);
        int buflen = strlen(buf);
        printf("buflen: %d\n", buflen);
        fwrite(buf, sizeof(char) * buflen, 1, f);
    }
    fclose(f);
    printf("Character data %s saved\n", name.c_str());

    printf("%s save succesfull!\n", buf);
    return 0;
}

int World::load_room(string r_name)
{
    if (r_name.size() < 1)
    {
        printf("given name too short, using room name\n");
        r_name = name;
    }
    char line[512];
    char buf[64];
    int  val1, val2, val3;
    sprintf(buf, "../maps/%s.bin", r_name.c_str());
    f = fopen(buf, "rb");
    if (!f)
    {
        printf("Could not load %s\n", buf);
        fclose(f);
        return 1;
    }
    fread(&current_room->tiles[0], sizeof(vector<unsigned int>::value_type), current_room->tiles.size(), f);
    fclose(f);
    printf("%s tiles loaded succesfully!\n", buf);

    sprintf(buf, "../maps/%s_characters.txt", r_name.c_str());
    f = fopen(buf, "r+");
    if (!f)
    {
        printf("Could not load %s\n", buf);
        fclose(f);
        return 2;
    }
    current_room->characters.clear();
    while(fgets(line, 512, f))
    {
        if (sscanf(line, "%d %d %d", &val1, &val2, &val3))
        {
            Character *c = new Character;
            c->id = val1;
            c->tex = characters[val1]->tex;
            c->name = characters[val1]->name;
            c->hp = 0;
            c->str = 0;
            c->x = val2;
            c->y = val3;
            current_room->characters.push_back(c);
            printf("%s added to room.\n", c->name.c_str());
            printf("current_room->characters.size(): %d\n",
                current_room->characters.size());
        }
    }
    fclose(f);
    return 0;
}

int World::load_characters(SDL_Renderer *renderer, Assets *assets)
{
    char line[512];
    char buf[64];
    int val;
    int idval;
    sprintf(buf, "../maps/characters.txt");
    f = fopen(buf, "r+");
    if (!f)
    {
        printf("Could not load %s\n", buf);
        fclose(f);
        return 1;
    }
    while(fgets(line, 512, f))
    {
        if (sscanf(line, "id: %d", &idval))
        {
            Character *c = new Character;
            c->id = idval;
            characters.push_back(c);
            printf("New character created ID: %d\n", idval);
            printf("World->characters.size(): %d\n", characters.size());
        }
        else if (sscanf(line, "name: %s", buf))
        {
            characters[idval]->name = buf;
            printf("Character ID %d name = %s\n", idval, buf);
        }
        else if (sscanf(line, "sprite: %s", buf))
        {
            assets->load_texture(renderer, buf);
            characters[idval]->tex = assets->textures[assets->textures.size() - 1];
            printf("Character ID %d tex = %s\n", idval, buf);
        }
        else if (sscanf(line, "hp: %d", &val))
        {
            characters[idval]->hp = val;
            printf("Character ID %d hp = %d\n", idval, val);
        }
        else if (sscanf(line, "str: %d", &val))
        {
            characters[idval]->str = val;
            printf("Character ID %d str = %d\n", idval, val);
        }
        else if (sscanf(line, "likes: %s", buf))
        {
            characters[idval]->likes = buf;
            printf("Character ID %d likes = %s\n", idval, buf);
        }
    }

    fclose(f);

    return 0;
}

int Room::add_character(Character *oc, int x, int y)
{
    Character *c = new Character;
    c->id = oc->id;
    c->tex = oc->tex;
    c->name = oc->name;
    c->likes = oc->likes;
    c->hp = oc->hp;
    c->str = oc->str;
    c->x = x;
    c->y = y;
    characters.push_back(c);
    printf("Char %s x: %d y: %d\n", c->name.c_str(), c->x, c->y);
    printf("Character %s added to room\n", c->name.c_str());
    return 0;
}
