#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <cstdlib>
#include "../src/globals.h"
#include "../src/graphics.h"
#include "../src/input.h"
#include "../src/game.h"

Game game;
bool g_mainloop;
int  SCREEN_WIDTH;
int  SCREEN_HEIGHT;
int  TILE_SIZE;

int main(int argc, char **argv)
{
    game.gamestate = GAMESTATE_MENU;
    SCREEN_WIDTH  = 1600;
    SCREEN_HEIGHT = 900;
    TILE_SIZE     = 32;
    g_mainloop    = 1;

    Input    input;
    Renderer renderer;
    Assets   assets;
    World    world;
    View     editor_view;

    if (renderer.init("Seikkailumaailma Editori") != 0) printf("Error in render.init\n");

    renderer.create_viewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, assets.c_background, "main");
    renderer.create_viewport(0, 0, TILE_SIZE * 25, TILE_SIZE * 20, assets.c_black, "editor");
    assets.load_texture(renderer.renderer, "../assets/tileset2.png");
    assets.load_texture(renderer.renderer, "../assets/grid_tile.png");
    assets.load_texture(renderer.renderer, "../assets/grid_tile2.png");
    assets.load_font(renderer.renderer, "../assets/consola.ttf", 16);
    assets.set_tileset_index(0);
    assets.generate_tileset_from_texture(assets.textures[0], TILE_SIZE);
    assets.create_button(SCREEN_WIDTH - 256, SCREEN_HEIGHT - 100, 64, 32, "tiles");
    assets.create_button(SCREEN_WIDTH - 184, SCREEN_HEIGHT - 100, 64, 32, "chars");
    assets.create_button(SCREEN_WIDTH - 112, SCREEN_HEIGHT - 100, 64, 32, "items");
    assets.create_button(SCREEN_WIDTH - 256, SCREEN_HEIGHT - 60, 64, 32, "doors");
    assets.create_button(SCREEN_WIDTH - 184, SCREEN_HEIGHT - 60, 64, 32, "save");
    assets.create_button(SCREEN_WIDTH  - 112, SCREEN_HEIGHT - 60, 64, 32, "load");
    world.create_room(100, 100); //TODO: Fix sizes that are not the same
    world.current_room = world.rooms[0];
    world.current_room->viewport = renderer.viewports[1];
    world.load_characters(renderer.renderer, &assets);

    while (g_mainloop)
    {
        input.editor_update(&game, &world, &assets);
        renderer.editor_update(&game, &world, &assets);
    }

    return 0;
}
