CC = cl
RM = del
TARGETS = main game graphics input
OBJECTS = build\intermediate\*.obj
COMPILER_FLAGS = /D_WANGBLOWS /Od /I $(INCLUDE_PATH) /W3 /EHsc /c
LINKER_FLAGS = /SUBSYSTEM:CONSOLE /MACHINE:X86
LIBS = SDL2main.lib SDL2.lib SDL2_image.lib SDL2_ttf.lib
EXE_NAME = seikkailumaailma.exe

.PHONY: run

all: $(TARGETS) link

link:
	LINK $(LINKER_FLAGS) $(OBJECTS) $(LIBS) /OUT:build\$(EXE_NAME)

main:
	$(CC) $(COMPILER_FLAGS) src\main.cpp /Fobuild\intermediate\main.obj

game:
	$(CC) $(COMPILER_FLAGS) src\game.cpp /Fobuild\intermediate\game.obj
graphics:
	$(CC) $(COMPILER_FLAGS) src\graphics.cpp /Fobuild\intermediate\graphics.obj
input:
	$(CC) $(COMPILER_FLAGS) src\input.cpp /Fobuild\intermediate\input.obj
run:
	cd build && $(EXE_NAME)
